# prunes

Cleans up empty subdirectories by removing them depth-first until files are found.

This is useful when removing lots of files leaves behind the clutter of empty subdirectories.

Some implementations scan a given directory for empty subdirectories. That's fine in certain use cases but limits use to a single root directory, and requires scanning the entire subtree, which may be costly in terms of I/O operations.

prunes works the other way around: Giving it a list of directories, nested or not. It scans only the directories from the list that could potentially be empty, deleting them until a non empty ancestor directory is found. This is just as accurate but much more efficient than an exhaustive search.

## API

### `prunes(...directories)`

Cleans up empty directories, travelling up the tree until a non-empty directory is encountered. Leaves intact any files or non-empty directories.

#### directories

One or more paths of directories to prune. Each will be resolved based on the current working directory.

## Use Case

```js
import {basedir} from 'path'
import prunes from 'prunes'

const files = [
  '/some/removed/file.txt',
  '/some/removed/other/file.txt',
  '/more/deprecated/files.txt',
  '/more/deprecated/unused/files.txt',
  '/more/old/files.txt'
  // ... lots more files that were unlinked ...
]

const directories = files.map((file) => basedir(file))

prunes(directories)
```

## Example

Given
```
beep
├── ity.js
└─┬ boop
  ├─┬ o_O
  │ ├─┬ oh
  │ │ ├── hello
  │ │ └── puny
  │ └── human.txt
  └── party
```

Running
```js
prunes(
  'beep/party',
  'beep/boop/o_O/oh/hello',
  'beep/boop/o_O/oh/puny'
)
```

Produces
```
beep
├── ity.js
└─┬ boop
  └─┬ o_O
    └── human.txt
```

## Peer Dependencies

This package uses Babel to transpile its code to be compatible with the node version it is installed on. Ensure you have installed the necessary peer dependencies.

```
npm install babel-cli babel-preset-env
```

On some older Node environments, e.g. 4.x and earlier, you may also require system-wide polyfills.

```
npm install babel-polyfill
```

And then load the `babel-polyfill` library before loading this package.

## Colophon

Made by Sebastiaan Deckers in Singapore 🇸🇬
