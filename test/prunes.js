const test = require('ava')
const mkdirp = require('mkdirp')
const rimraf = require('rimraf')
const touch = require('touch')
const { promisify } = require('util')
const isDirectory = require('is-directory')
const isFile = require('is-file')

const prunes = require('..')

test.beforeEach(async (t) => {
  const fixtures = [
    'fixtures/foo/bar',
    'fixtures/lol/abc/def'
  ]
  for (const directories of fixtures) {
    await promisify(mkdirp)(directories)
  }
})

test.serial('prunes until file', async (t) => {
  await promisify(touch)('fixtures/foo/hello.txt')

  await prunes('fixtures/foo/bar')

  t.truthy(await promisify(isFile)('fixtures/foo/hello.txt'))
  t.falsy(await promisify(isDirectory)('fixtures/foo/bar'))
})

test.serial('prunes one empty', async (t) => {
  await prunes('fixtures/lol/abc/def')

  t.falsy(await promisify(isDirectory)('fixtures/lol'))
})

test.serial('prunes all empty', async (t) => {
  await prunes(
    'fixtures',
    'fixtures/lol/abc/def',
    'fixtures/foo/bar'
  )

  t.falsy(await promisify(isDirectory)('fixtures'))
})

test.afterEach.always(async (t) => {
  await promisify(rimraf)('fixtures')
})
