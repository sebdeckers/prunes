const { sep: separator, resolve, dirname } = require('path')
const { rmdir } = require('fs')
const { promisify } = require('util')

module.exports = async function prunes (...directories) {
  const resolved = directories.map((directory) => resolve(directory))
  const uniques = Array.from(new Set(resolved))
  const leaves = uniques.filter((directory) =>
    uniques.every((branch) =>
      !branch.startsWith(directory + separator)))
  for (let leaf of leaves) {
    for (; leaf; leaf = dirname(leaf)) {
      if (leaf === separator) break
      try {
        await promisify(rmdir)(leaf)
      } catch (err) {
        if (err.code === 'ENOTEMPTY') break
        else throw err
      }
    }
  }
}
